module Main where

import Options.Applicative
import Data.Maybe
import Control.Monad
import qualified Data.ByteString as B
import qualified Data.Text as T
import Data.Text.Encoding

data Opts = Opts
    { l :: Bool,
      c :: Bool,
      m :: Bool,
      w :: Bool,
      filename :: Maybe String
    }

runWithOptions :: Opts -> IO ()
runWithOptions opts = do
    contents <- if isNothing (filename opts) then getContents else readFile (fromJust (filename opts))
    when (l opts || noFlags) $ print (length (lines contents))
    when (w opts || noFlags) $ print (length (words contents)) 
    when (m opts || noFlags) $ print (length contents)
    when (c opts || noFlags) $ print (B.length (encodeUtf8 (T.pack contents)))
    where
        noFlags = (not (l opts)) && (not (c opts)) && (not (m opts)) && (not (w opts))

main :: IO ()
main = execParser opts >>= runWithOptions where
    parser = Opts <$> switch (short 'l' <>
                               long "lines" <>
                               help "The number of lines is written to the standard output.")
                    <*> switch (short 'c' <>
                               long "bytes" <>
                               help "The number of bytes is written to the standard output.")
                    <*> switch (short 'm' <>
                               long "chars" <>
                               help "The number of characters is written to the standard output.")
                    <*> switch (short 'w' <>
                               long "words" <>
                               help "The number of words is written to the standard output.")
                    <*> optional (argument str (metavar "filename"))
                    <**> helper
    opts = info parser mempty